//package com.molyfun.web.student;
//import com.molyfun.core.base.BaseController;
//import com.molyfun.core.support.Assert;
//import com.molyfun.core.support.HttpCode;
//import com.molyfun.core.util.SecurityUtil;
//import com.molyfun.model.student.MhStudent;
//import com.molyfun.model.student.MhStudentBean;
//import com.molyfun.service.student.MhStudentServiceImpl;
//import com.molyfun.web.base.BaseWebController;
//import com.molyfun.web.base.UserThreadLocal;
//
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//
///**
// *  用户登录
// *  shidongsheng
// */
//@RequestMapping("/api/v1/user")
//@RestController
//@Api(description = "用户接口")
//public class StudentController extends BaseWebController {
//
//	// 登录
//	@ApiOperation(value = "用户登录")
//	@RequestMapping(value = "/login",method = RequestMethod.POST)
//	public Object login(@ApiParam(required = true, value = "需要参数:账号,密码") @RequestBody MhStudentBean bean) {
//		Assert.isNotBlank(bean.getAccount(), "ACCOUNT");
//		Assert.isNotBlank(bean.getPassword(), "PASSWORD");
//		MhStudent student = studentService.login(bean.getAccount(), bean.getPassword());
//		if(student == null)
//		{
//			return setModelMap(HttpCode.BUSINESS_FAIL,"用户名或密码不正确");
//		}
//		String token = SecurityUtil.encryptMd5(SecurityUtil.encryptSHA(System.currentTimeMillis() + student.getAccount()+student.getPassword()));
//		student.setToken(token);
//		studentService.update(student);
//		student.setPassword(null);
//		return setSuccessModelMap(student);
//	}
//	
//	
//	// 登录
//	@ApiOperation(value = "用户信息")
//	@GetMapping("/info/{id}")
//	public Object info(@ApiParam(required = true, value = "用户ID") @PathVariable("id") String id) {
//		checkToken(id);
//		return setSuccessModelMap(UserThreadLocal.get());
//	}
//	
//	
//	
//	// 登出
//	@ApiOperation(value = "用户登出")
//	@GetMapping("/logout/{id}")
//	public Object logout(@ApiParam(required = true, value = "用户ID") @PathVariable("id") String id) {
//		checkToken(id);
//		MhStudent student = UserThreadLocal.get();
//		student.setToken("");
//		studentService.update(student);
//		return setSuccessModelMap("");
//	}
//	
//	
//	
//	// 修改密码
//	@ApiOperation(value = "修改密码")
//	@RequestMapping(value = "/updatepassword/{id}",method = RequestMethod.POST)
//	public Object updatepassword( @ApiParam(required = true, value = "用户ID") @PathVariable("id") String id,
//			@ApiParam(required = true, value = "需要参数:密码") @RequestBody MhStudentBean bean) {
//		Assert.isNotBlank(bean.getPassword(), "password参数为空");	
//		checkToken(id);
//		studentService.updatePassword(bean);
//		return setSuccessModelMap("");
//	}
//	
//	
//	
//	
////	// 修改密码
////	@ApiOperation(value = "修改密码")
////	@RequestMapping(value = "/update/password")
////	public Object updatePassword(ModelMap modelMap, @RequestParam(value = "id", required = false) Integer id,
////			@RequestParam(value = "password", required = false) String password) {
////		sysUserService.updatePassword(id, password);
////		return setSuccessModelMap(modelMap);
////	}
//}
package com.molyfun.web.student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.molyfun.bean.student.MhStudentBean;
import com.molyfun.core.annotation.Authorization;
import com.molyfun.core.base.BaseController;
import com.molyfun.core.support.Assert;
import com.molyfun.model.student.MhStudent;
import com.molyfun.service.student.MhStudentServiceImpl;

@RequestMapping("/api/v1/student")
@RestController
@Api(description = "学生接口")
   public class StudentController extends BaseController{
	   
	@Autowired
	public MhStudentServiceImpl studentService;
	   
	
	   //获取学生邀请码
	   @ApiOperation(value = "获取邀请码")
	   @GetMapping("/inviteCode/{cardId}")
	   @Authorization
	   @ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "header")
	   public Object  inviteCode(@ApiParam(required = true, value = "需要参数:学生身份证号") @PathVariable("cardId")String cardId){
		   Assert.isNotBlank(cardId,"身份证号为空");
		  
		   String code=studentService.findInviteCode(cardId);
		   return setSuccessModelMap(code); 
	   }
	   
	   //将邀请码与学生关联
		@ApiOperation(value = "关联邀请码")
		@RequestMapping(value = "/saveInviteCode/{inviteCode}",method = RequestMethod.POST)
		@Authorization
		@ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "header")
		public Object saveInviteCode( @ApiParam(required = true, value = "需要参数:学生身份证号") @PathVariable("cardId") String cardId,
				@ApiParam(required = true, value = "需要参数:邀请码") @RequestBody MhStudentBean msb) {
			Assert.isNotBlank(msb.getInviteCode(), "邀请码为空");
			Assert.isNotBlank(cardId,"身份证号码为空");
			studentService.saveInviteCode(cardId,msb);
			return setSuccessModelMap("");
		}
		
		
		//获取学生信息 (根据身份证号)
		@ApiOperation(value = "获取学生信息")
		@GetMapping("/findStudentInfo/{cardId}")
		@Authorization
		@ApiImplicitParam(name = "token", value = "token", required = true, dataType = "string", paramType = "header")
		 public ResponseEntity<ModelMap>  findStudentInfo(@ApiParam(required = true, value = "需要参数:学生身份证号") @PathVariable("cardId") String cardId){
			 Assert.isNotBlank(cardId,"身份证号为空");
			 MhStudent stuInfo=studentService.findStudentInfo(cardId);
			 return setSuccessModelMap(stuInfo); 
			
		}
	   
   }









