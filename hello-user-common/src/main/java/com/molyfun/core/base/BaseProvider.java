package com.molyfun.core.base;

import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageInfo;

/**
 */
public interface BaseProvider<T extends BaseModel> {
	@Transactional
	public T add(T record);
	
	@Transactional
	public T update(T record);

	@Transactional
	public void delete(String id, String userId);

	public T queryById(String id);

	public void init();

	public PageInfo<T> query(Map<String, Object> params);
}
