package com.molyfun.core.interceptor;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.ModelMap;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.molyfun.core.support.HttpCode;

import java.io.IOException;


/**
 * 404错误验证
 * 石冬生
 * 2016.7.12
 * 
 */
public class NoMappingFoundServlet extends HttpServlet {
    private static final long serialVersionUID = -1257947018545327308L;

	@Override
	public void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {

		//验证
		ModelMap modelMap = new ModelMap();
		modelMap.put("code", HttpCode.BUSINESS_FAIL.value());
		modelMap.put("msg", "非法请求");
		response.setContentType("application/json;charset=UTF-8");
		byte[] bytes = JSON.toJSONBytes(modelMap, SerializerFeature.DisableCircularReferenceDetect);
		response.getOutputStream().write(bytes);
	}

	@Override
	public void doPost(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {
		doGet(request, response);
	}


}