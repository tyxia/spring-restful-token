package com.molyfun.model.student;


/**

 * 创建日期：2016-7-8下午5:38:11

 * 作者：石冬生

 */
public class HoUserBean{

	private String username;
	
    private String phone;

    private String password;
    
    private String headimgurl;
    
    private String captchacode;

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the headimgurl
	 */
	public String getHeadimgurl() {
		return headimgurl;
	}

	/**
	 * @param headimgurl the headimgurl to set
	 */
	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}

	/**
	 * @return the captchacode
	 */
	public String getCaptchacode() {
		return captchacode;
	}

	/**
	 * @param captchacode the captchacode to set
	 */
	public void setCaptchacode(String captchacode) {
		this.captchacode = captchacode;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}


	
	
	
}
